// smooth-scroll
$.smoothScroll({
    //æ»å¨å°çä½ç½®çåç§»é
    offset: 0,
    //æ»å¨çæ¹åï¼å¯å 'top' æ 'left'
    direction: 'top',
    // åªæå½ä½ æ³éåé»è®¤è¡ä¸ºçæ¶åæä¼ç¨å°
    scrollTarget: null,
    // æ»å¨å¼å§åçåè°å½æ°ã`this` ä»£è¡¨æ­£å¨è¢«æ»å¨çåç´ 
    beforeScroll: function () { },
    //æ»å¨å®æåçåè°å½æ°ã `this` ä»£è¡¨è§¦åæ»å¨çåç´ 
    afterScroll: function () { },
    //ç¼å¨ææ
    easing: 'swing',
    //æ»å¨çéåº¦
    speed: 700,
    // "èªå¨" å éçç³»æ°
    autoCoefficent: 2
});


// Bind the hashchange event listener
$(window).bind('hashchange', function (event) {
    $.smoothScroll({
        // Replace '#/' with '#' to go to the correct target
        offset: $("body").attr("data-offset")? -$("body").attr("data-offset"):0 ,
        // offset: -30,
        scrollTarget: decodeURI(location.hash.replace(/^\#\/?/, '#'))
        
      });
});

// $(".smooth-scroll").on('click', "a", function() {
$('a[href*="#"]')
    .bind('click', function (event) {    
    // Remove '#' from the hash.
    var hash = this.hash.replace(/^#/, '')
    if (this.pathname === location.pathname && hash) {
        event.preventDefault();
        // Change '#' (removed above) to '#/' so it doesn't jump without the smooth scrolling
        location.hash = '#/' + hash;
    }
});

// Trigger hashchange event on page load if there is a hash in the URL.
if (location.hash) {
    $(window).trigger('hashchange');
}

// // $('[data-spy="scroll"]').each(function () {
// //     var $spy = $(this).scrollspy('refresh')
// //   })

// $('[data-spy="scroll"]').on('activate.bs.scrollspy', function () {
//     // do somethingâ¦
//     var offset = $('[data-spy="scroll"]').attr("data-offset")
//   })