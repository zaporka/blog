+++
showonlyimage = true
date = "2018-08-03"
title = "Scrap of book..."
math = "true"
image = "images/profile.jpg"
tags = [
    "go",
    "golang",
    "templates",
    "themes",
    "development",
]
categories = [
    "Development",
    "golang",
]

+++

## I SAW

I saw the freedom among the azure skies

The sough of grain was dispersing the clouds

I saw wealth constructed of  gold walls - worth nothing

Your eyes depicted in my mind 

There is condensed happiness in them to find

I didn’t want to believe in marvel when there was one...
